import { Injectable } from '@angular/core';
import { Scene, Game, Types, AUTO, Scale, Physics, GameObjects } from 'phaser'
import { Player } from "@kiliandeca/game-common"
import { SocketService } from './socket.service';
import { interval } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MainSceneService extends Scene {

  private player = new Player()
  private otherPlayers: Player[] = []
  private otherPlayersSprites = new Map<string, Types.Physics.Arcade.SpriteWithDynamicBody>();

  private playerSprite!: Types.Physics.Arcade.SpriteWithDynamicBody
  private cursors!: Types.Input.Keyboard.CursorKeys

  constructor(private socketService: SocketService) {
    super({ key: 'main' });
    console.log(socketService);
    
    //this.socketService.sendPlayerPosition(this.player)
    this.socketService.playersPositions().subscribe(players => {
      this.otherPlayers = players.filter(p => p.id != this.player.id)
    })

  }

  preload() {
    this.load.image('bg', 'assets/background.jpg');
    this.load.image('doge', 'assets/doge.png');
  }
  create() {

    this.player.id = Math.floor(Math.random()*100).toString()
    this.player.name = 'bob'
    this.player.position = {
      x: 0,
      y: 0
    }
    this.player.velocity = {
      x: 0,
      y: 0
    }
    interval(100).subscribe(e => this.socketService.sendPlayerPosition(this.player))


    const worldSizeX = 1920 * 2
    const worldSizeY = 1080 * 2

    this.cameras.main.setBounds(0, 0, worldSizeX, worldSizeY);
    this.physics.world.setBounds(0, 0, worldSizeX, worldSizeY);


    const backgroundImage = this.add.image(0, 0, 'bg').setOrigin(0)
    const scale = Math.max(worldSizeX / backgroundImage.width, worldSizeY / backgroundImage.height)
    backgroundImage.setScale(scale)

    this.playerSprite = this.physics.add.sprite(100, 450, 'doge');
    //this.player = this.add.circle(200, 200, 50, 0xff0000)
    //this.player = new Physics.Arcade.Body(this.physics.world, circle)
    //this.player =this.physics.add.existing(this.player)

    //this.player.setBounce(0.2, 0.2);
    this.playerSprite.setCollideWorldBounds(true);

    this.cameras.main.startFollow(this.playerSprite, true, 0.05, 0.05);

    this.cursors = this.input.keyboard.createCursorKeys();
  }

  update() {
    this.playerSprite.setVelocity(0);

    if (this.cursors.left.isDown) {
      this.playerSprite.setVelocityX(-500);
      this.playerSprite.setFlipX(true)
    }
    else if (this.cursors.right.isDown) {
      this.playerSprite.setVelocityX(500);
      this.playerSprite.setFlipX(false)
    }

    if (this.cursors.up.isDown) {
      this.playerSprite.setVelocityY(-500);
    }
    else if (this.cursors.down.isDown) {
      this.playerSprite.setVelocityY(500);
    }
    
    this.updatePlayerPosition()
    
    this.otherPlayers.forEach(player => {
      
      let sprite = this.otherPlayersSprites.get(player.id)
      if (!sprite) {
        sprite = this.physics.add.sprite(100, 450, 'doge');
        this.otherPlayersSprites.set(player.id, sprite)
      }

      sprite.setX(player.position.x)
      sprite.setY(player.position.y)
      sprite.setVelocityX(player.velocity.x)
      sprite.setVelocityY(player.velocity.y)
      if (player.velocity.x > 0) {
        sprite.setFlipX(false)
      } else if (player.velocity.x < 0) {
        sprite.setFlipX(true)
      }
    })
  }

  updatePlayerPosition(){
    this.player.position.x = this.playerSprite.x
    this.player.position.y = this.playerSprite.y
    this.player.velocity.x = this.playerSprite.body.velocity.x
    this.player.velocity.y = this.playerSprite.body.velocity.y    
  }

}
