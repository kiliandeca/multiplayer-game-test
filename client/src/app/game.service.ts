import { Injectable } from '@angular/core';
import { Scene, Game, Types, AUTO, Scale, Physics, GameObjects } from 'phaser'
import { MainSceneService } from './main-scene.service';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  game!: Game;

  constructor(private mainSceneService: MainSceneService) {
    const config = {
      type: AUTO,
      scale: {
        mode: Scale.FIT,
        autoCenter: Scale.CENTER_BOTH,
        height: window.innerHeight,
        width: window.innerWidth
      },
      scene: [mainSceneService],
      parent: 'gameContainer',
      physics: {
        default: 'arcade',
      }
    };

    this.game = new Phaser.Game(config);
  }
}
