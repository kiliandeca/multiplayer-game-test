import { Injectable } from '@angular/core';
import { interval, Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { Socket } from 'ngx-socket-io';
import { Player } from '@kiliandeca/game-common';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SocketService extends Socket{

  constructor() {
    super({url: environment.socketUrl, options: {}})
    this.fromEvent("message").subscribe(e => console.log(e))
  }

  sendPlayerPosition(player: Player){
    this.emit('playerPosition', player.serialize())
  }

  playersPositions(): Observable<Player[]> {
    return this.fromEvent('playersPositions').pipe(
      map((rawArray: any) => {          
          const result = JSON.parse(rawArray) as []
          
          return result.map(p => Player.deserialize(JSON.stringify(p)))
      })
    )
  }
}
