import { serialize, deserialize, deserializeArray } from "class-transformer"

export class Player {
    id!: string;
    name!: string;
    position!: {
        x: number,
        y: number
    };
    velocity!: {
        x: number,
        y: number
    }

    constructor(){}

    serialize(): string{
        return serialize(this)
    }

    static deserialize(json: string): Player {        
        return deserialize(Player, json)
    }

    static deserializeArray(json: string): Player[] {        
        return deserializeArray(Player, json)
    }
}