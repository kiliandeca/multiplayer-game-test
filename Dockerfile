FROM node:14

WORKDIR /app

COPY node_modules node_modules
COPY common node_modules/@kiliandeca/game-common
COPY server .

COPY client/dist/game-client client

ENTRYPOINT [ "node", "dist/main.js" ]