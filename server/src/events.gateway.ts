import { SubscribeMessage, WebSocketGateway, OnGatewayConnection, WebSocketServer } from '@nestjs/websockets';
import { Player } from '@kiliandeca/game-common'
import { Client, Server } from 'socket.io'
import { interval } from 'rxjs';

@WebSocketGateway()
export class EventsGateway {

  @WebSocketServer()
  server: Server;

  players: Player[] = []

  constructor() {
    interval(100).subscribe(e => {
      this.server.emit('playersPositions', JSON.stringify(this.players))
    })
    
  }

  @SubscribeMessage('playerPosition')
  handlePlayerPosition(client: Client, payload: any): string {
    
    const player = Player.deserialize(payload)


    const index = this.players.findIndex(p => p.id === player.id)
    if (index == -1) {
      this.players.push(player);
    client.conn.on('disconnect', e => console.log(e));

      return;
    }

    this.players[index] = player
    
    return 'Hello world!';
  }
}
